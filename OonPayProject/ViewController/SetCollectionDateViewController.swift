//
//  SetCollectionDateViewController.swift
//  OonPayProject
//
//  Created by Ankita Patil on 19/10/20.
//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit

class SetCollectionDateViewController: UIViewController{
 
    
  //Mark:IBOUTLETS
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var DateCollectionView: UICollectionView!
    
    @IBOutlet weak var collectionPopupView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource=self
        
        DateCollectionView.delegate = self
        DateCollectionView.dataSource=self
    }
    
    
    @IBAction func actionBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func actionStartNowBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CollectionsViewController") as! CollectionsViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
}
extension SetCollectionDateViewController : UICollectionViewDelegate,UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = DateCollectionView.dequeueReusableCell(withReuseIdentifier: "SetCollectionDateViewCell", for: indexPath)
        return cell 
    }
    
    
}

extension SetCollectionDateViewController:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SetCollectionDateTableViewCell") as! SetCollectionDateTableViewCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        collectionPopupView.isHidden=false
    }
     
    
}
