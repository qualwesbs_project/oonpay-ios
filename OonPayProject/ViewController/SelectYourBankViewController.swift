//
//  SelectYourBankViewController.swift
//  OonPayProject
//
//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit

class SelectYourBankViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource=self
        tableView.delegate = self
    }
    
    
    @IBAction func actionBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}




extension SelectYourBankViewController:UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:SelectBankListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SelectBankListTableViewCell") as! SelectBankListTableViewCell
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BankAccountDetailViewController") as! BankAccountDetailViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
