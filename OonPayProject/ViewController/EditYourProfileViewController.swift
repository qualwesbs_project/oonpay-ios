//
//  EditYourProfileViewController.swift
//  OonPayProject
//

//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit

class EditYourProfileViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
 //Mark:- IBoutlets
    @IBOutlet weak var registeredPhoneNoLabel: DesignableUILabel!
    @IBOutlet weak var businessNameLabel: DesignableUILabel!
    @IBOutlet weak var businessEmailLabel: DesignableUILabel!
    @IBOutlet weak var businessCategoryLabel: DesignableUILabel!
    @IBOutlet weak var businessLocation: DesignableUILabel!
    @IBOutlet weak var imageView: ImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
          SessionManager.shared.methodForApiCalling(url: U_BASE + U_EDIT_BUSINESS, method: .get, parameter: nil, objectClass: EditBusiness.self, requestCode: U_EDIT_BUSINESS) { (response)in
            DispatchQueue.main.async {
                let arr = response.response!.business
                      self.businessNameLabel.text = "\(arr?.business_name! ?? "")"
                      self.businessLocation.text = "\(arr?.business_location ?? "")"
                      self.businessEmailLabel.text = "\(arr?.business_email ?? "")"
                      self.registeredPhoneNoLabel.text = "\(arr?.business_contact ?? "")"
                      self.businessCategoryLabel.text = "\(arr?.business_category ?? "")"
                
            }
               
         
        }
     
    }
 //IBAction--

@IBAction func actionBackbtn(_ sender: Any) {
   self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionBusinessPhotoBtn(_ sender: Any) {
//        let upload_image = imageView.image!
//
//        ActivityIndicator.show(view: self.view)
//
//        let param:[String:Any]=["image" : upload_image ?? "" ,
//                                "type": "" ]
//        let imagePickerCon = UIImagePickerController()
//            imagePickerCon.delegate = self;
//            imagePickerCon.sourceType = UIImagePickerController.SourceType.photoLibrary
//        self.present(imagePickerCon, animated: true, completion: nil)
//
//
//
//        //   protocol methods-------------------
//
//           func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
//               picker.dismiss(animated: true, completion: nil)
//           }
//
//           func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
//           {
//               print("Received....", info)
//               picker.dismiss(animated: true, completion: nil)
//
//
//               //get selected images......
//               let selectImage : UIImage = info[UIImagePickerController.InfoKey.editedImage] as! UIImage
//
//               imageView.image = selectImage
//           }
//        SessionManager.shared.makeMultipartRequest(url: U_Upload_Image, fileData: , param: param, fileName: "", completionHandler: <#T##(Any) -> Void#>)
        
    }
        
//    let BusinessPhotoVc = self.storyboard?.instantiateViewController(withIdentifier: "UploadProfilePictureViewController") as! UploadProfilePictureViewController
//
//  self.navigationController?.pushViewController(BusinessPhotoVc, animated: true)
    
    
    @IBAction func actionEditNameBtn(_ sender: Any) {
    }
    
    @IBAction func actionBusinessNameBtn(_ sender: Any) {
    }
    
    @IBAction func actionRegistereMobileno(_ sender: Any) {
    }
    
    @IBAction func actionBussinessEmail(_ sender: Any) {
    }
    
    @IBAction func actionBusinessCategory(_ sender: Any) {
    }
    
    
    @IBAction func actionBusinessLocation(_ sender: Any) {
    }
}

