//
//  CollectionsViewController.swift
//  OonPayProject
//
//  Created by Ankita Patil on 20/10/20.
//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit

class CollectionsViewController: UIViewController  {

    var  pageControlVC:PageViewController!
   
    override func viewDidLoad() {
        super.viewDidLoad()
}
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "pagesegue"
        {
            pageControlVC = segue.destination as! PageViewController
            
        }
    }

    @IBAction func actionBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}


