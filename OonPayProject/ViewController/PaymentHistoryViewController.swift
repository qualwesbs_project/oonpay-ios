//
//  PaymentHistoryViewController.swift
//  OonPayProject
//
//  Created by Ankita Patil on 03/11/20.
//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit

class PaymentHistoryViewController: UIViewController {
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource=self
        collectionView.delegate = self
    }
   
    
    @IBAction func actionOlderdateBtn(_ sender: Any) {
        let OlderdateVC  = self.storyboard?.instantiateViewController(withIdentifier: "OlderDatesViewController") as! OlderDatesViewController
        self.navigationController?.pushViewController(OlderdateVC, animated: true)
    }
    
    
    
    @IBAction func actionBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    

    @IBAction func actionViewBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PendingMoneyRequestViewController") as! PendingMoneyRequestViewController
        self.navigationController?.pushViewController(vc, animated:     true)
    }
}

extension  PaymentHistoryViewController : UICollectionViewDelegate , UICollectionViewDataSource {
    

func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return 5
}

func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PaymentHistoryCollectionViewCell", for: indexPath) as! PaymentHistoryCollectionViewCell
    return cell
}

}
