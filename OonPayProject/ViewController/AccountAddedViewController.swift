//
//  AccountAddedViewController.swift
//  OonPayProject
//
//  Created by Ankita Patil on 06/11/20.
//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit

class AccountAddedViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionDoneBtn(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BankAccountViewController") as! BankAccountViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

}
