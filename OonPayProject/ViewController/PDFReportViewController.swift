//
//  PDFReportViewController.swift
//  OonPayProject
//

//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit

class PDFReportViewController: UIViewController {

//Mark:-Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var PopupView1: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate=self
        tableView.dataSource=self
    }
    
//Mark:-IBAction
  
    @IBAction func actionBackBtn(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionDropDownBtn(_ sender: Any) {
        PopupView1.isHidden=false
    }
    
    
   
    
    
}
extension PDFReportViewController:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ShowItemsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ShowItemsTableViewCell") as! ShowItemsTableViewCell
        return cell
    }
    
    
}
