//
//  SeeProfileViewController.swift
//  OonPayProject
//

//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit

class SeeProfileViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
//Mark:- IBOutlets--
    
    
//Mar :- IBAction-
    
    @IBAction func actionBtnRequestMoney(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RequestMoneyViewController") as! RequestMoneyViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionPaymentHistory(_ sender: Any) {
        let paymentHistoryVC = self.storyboard?.instantiateViewController(withIdentifier: "PaymentHistoryViewController") as! PaymentHistoryViewController
        self.navigationController?.pushViewController(paymentHistoryVC, animated: true)
 
    }
    
    @IBAction func actionQRCodeViewController(_ sender: Any) {
        
        let QRCodeVC = self.storyboard?.instantiateViewController(withIdentifier: "QRCodeViewController") as! QRCodeViewController
        self.navigationController?.pushViewController(QRCodeVC, animated: true)
    }
    
    
    @IBAction func actionBusinessCardBtn(_ sender: Any) {
        let BusinessCardVC = self.storyboard?.instantiateViewController(withIdentifier: "BusinessCardViewController") as! BusinessCardViewController
        self.navigationController?.pushViewController(BusinessCardVC, animated: true)
        
    }
    
    
    @IBAction func actionPaymentLinkBtn(_ sender: Any) {
        let PaymentLinkVC = self.storyboard?.instantiateViewController(withIdentifier: "SendPaymentLinkToCustomerViewController") as! SendPaymentLinkToCustomerViewController
        self.navigationController?.pushViewController(PaymentLinkVC, animated: true)
    }
    
    
    @IBAction func actionEditProfileBtn(_ sender: Any) {
        let EditVC=self.storyboard?.instantiateViewController(withIdentifier: "EditYourProfileViewController") as! EditYourProfileViewController
        self.navigationController?.pushViewController(EditVC, animated: true)
    }
    
    
    @IBAction func actionUploadProfileBtn(_ sender: Any) {
        
        
        
        
        
        
//        let ProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "UploadProfilePictureViewController") as! UploadProfilePictureViewController
//        self.navigationController?.pushViewController(ProfileVC, animated: true)
        
    }
    
    
    
    
}
