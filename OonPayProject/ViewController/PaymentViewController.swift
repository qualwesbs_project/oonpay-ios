//
//  PaymentViewController.swift
//  OonPayProject
//
//  Created by Ankita Patil on 14/10/20.
//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit

class PaymentViewController: UIViewController {

   //Mark:-Outlets
    @IBOutlet weak var userImage: ImageView!
    @IBOutlet weak var username: DesignableUILabel!
    @IBOutlet weak var userNumber: DesignableUILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var customer = Customer()
    var customerDetail = SingleCustomerResponse()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource=self
        tableView.delegate=self
        self.getSingleCustomer()
        // Do any additional setup after loading the view.
    }
    
    func getSingleCustomer(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_SINGLE_CUSTOMER + "\(self.customer.id ?? 0)", method: .post, parameter: nil, objectClass: GetSingleCustomer.self, requestCode: U_GET_SINGLE_CUSTOMER) { (response) in
            self.customerDetail = response.response
            self.username.text = self.customerDetail.customer[0].customer_name
            self.userNumber.text = self.customerDetail.customer[0].customer_mobile_number
            self.tableView.reloadData()
            ActivityIndicator.hide()
        }
    }
    
//Mark:- Actions
    @IBAction func actionGiveCreditViewController(_ sender: Any) {
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "EnterCreditTransactionViewController") as! EnterCreditTransactionViewController
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    
    @IBAction func actionBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func actionReportBtn(_ sender: Any) {
        let PdfReportVC = self.storyboard?.instantiateViewController(withIdentifier: "PDFReportViewController") as! PDFReportViewController
        self.navigationController?.pushViewController(PdfReportVC, animated: true)
    }
    
    @IBAction func actionPaymentBtn(_ sender: Any) {
        let PaymentVC = self.storyboard?.instantiateViewController(withIdentifier: "AddBankAccountViewController") as! AddBankAccountViewController
        self.navigationController?.pushViewController(PaymentVC, animated: true)
        
    }
    
    @IBAction func actionSMSBtn(_ sender: Any) {
        let smsVC = self.storyboard?.instantiateViewController(withIdentifier: "SendSMSViewController") as! SendSMSViewController
        self.navigationController?.pushViewController(smsVC, animated: true)
    }
    
    @IBAction func actionReminderVC(_ sender: Any) {
        let reminderVC = self.storyboard?.instantiateViewController(withIdentifier: "SharePaymentReminderViewController") as! SharePaymentReminderViewController
        self.navigationController?.pushViewController(reminderVC, animated: true)
    }
    
}
extension PaymentViewController:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.customerDetail.customer_products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ListTableViewCell") as! ListTableViewCell
        let val = self.customerDetail.customer_products[indexPath.row]
        cell.DateTimeLabel.text = self.convertTimestampToDate(val.receipt_date ?? 0, to: "dd MMM yy, h:mm a")
        cell.priceLabel.text = "₦\(val.price ?? 0)"
        cell.itemLabel.text = val.product_name
        
        return cell
    }
 
    
    
    
    
    
    
    
    
    
    
}
