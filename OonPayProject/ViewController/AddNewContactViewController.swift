//
//  AddContactViewController.swift
//  OonPayProject
//
//  Created by Ankita Patil on 17/10/20.
//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit

class AddNewContactViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var customerName: CustomTextField!
    @IBOutlet weak var customerNumber: DesignableUITextField!
    @IBOutlet weak var customerEmail: CustomTextField!
    
    var selectedContact = PhoneContact()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(self.selectedContact.name != nil){
            self.customerName.text = self.selectedContact.name
            if(self.selectedContact.phoneNumber.count > 0){
            self.customerNumber.text = self.selectedContact.phoneNumber[0]
            }
        }
    }
    
    
    
    //MARK: IBActions
    @IBAction func actionNextBtn(_ sender: Any) {
        if(customerName.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter Customer Name")
        }else if(customerNumber.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter Customer Number")
        }else if(customerEmail.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter Customer Email")
        }else {
            ActivityIndicator.show(view: self.view)
            let param:[String:Any] = [
                "customer_name": self.customerName.text,
                "customer_mobile_number": self.customerNumber.text,
                "customer_email": self.customerEmail.text,
            ]
            
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_ADD_CUSTOMER, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_ADD_CUSTOMER) { (response) in
                Singleton.shared.showToast(text: "Successfully added Customer")
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    @IBAction func actionBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
