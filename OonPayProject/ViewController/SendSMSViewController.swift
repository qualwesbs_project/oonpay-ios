//
//  SendSMSViewController.swift
//  OonPayProject
//
//  Created by Ankita Patil on 06/11/20.
//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit

class SendSMSViewController: UIViewController {
    
//Mark:- Outlets
    
    @IBOutlet weak var SmsPopupView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        SmsPopupView.isHidden=false
        
    }

}
