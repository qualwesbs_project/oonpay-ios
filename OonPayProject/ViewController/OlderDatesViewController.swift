//
//  OlderDatesViewController.swift
//  OonPayProject
//
//  Created by Ankita Patil on 04/11/20.
//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit

class OlderDatesViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource=self
        tableView.delegate = self

    }
    

    @IBAction func actionBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
extension OlderDatesViewController : UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OlderDatesTableViewCell") as!  OlderDatesTableViewCell
        return cell
        
    }
    
    
}
