//
//  TabBarViewController.swift
//  OonPayProject
//

//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.selectedImageTintColor = .blue
    }
}
