//
//  PageViewController.swift
//  OonPayProject
//
//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit

class PageViewController: UIPageViewController
{
    static var dataSource: UIPageViewControllerDataSource?
    var requestsViewContorllers: [UIViewController] = []
    var pageControl = UIPageControl()
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        handleView()
        setControllers()
        self.dataSource = self
        self.delegate = self
        PageViewController.dataSource = self
        for view in self.view.subviews {
                   if let subView = view as? UIScrollView {
                           subView.isScrollEnabled = true
                   }
               }
    }
    
    func setControllers() {
            if let firstViewController = requestsViewContorllers.first {
                setViewControllers([firstViewController],
                                   direction: .reverse,
                                   animated: false,
                                   completion: nil)
            }
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func viewDidAppear(_ animated: Bool) {
        PageViewController.dataSource = self
    }
    
    func handleView() {
      requestsViewContorllers = [self.newColoredViewController(controller: "PendingViewController"),
         self.newColoredViewController(controller: "TodayViewController"),
         self.newColoredViewController(controller: "UpcomingViewController")]
    }
    
    func setControllerFirst() {
        if let firstViewController = requestsViewContorllers.first {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: false,
                               completion: nil)
        }
    }
    
    func setControllerFirstReverse() {
        if let firstViewController = requestsViewContorllers.first {
            setViewControllers([firstViewController],
                               direction: .reverse,
                               animated: false,
                               completion: nil)
        }
    }
    
    func setControllerSecond() {
        setViewControllers([requestsViewContorllers[1]], direction: .forward, animated: false, completion: nil)
    }
    

    
    func setControllerLast() {
        if let lastViewController = requestsViewContorllers.last {
            setViewControllers([lastViewController],
                               direction: .forward,
                               animated: false,
                               completion: nil)
        }
    }
    

    
    private func newColoredViewController(controller: String)->UIViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: controller)
    }
}
extension PageViewController: UIPageViewControllerDataSource
{
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
      //  currentControllerIndex(VC: viewController)
        guard let viewControllerIndex = requestsViewContorllers.index(of: viewController) else {
            return nil
        }
        let previousIndex = viewControllerIndex - 1
        guard previousIndex >= 0 else {
            return nil
        }
        guard requestsViewContorllers.count > previousIndex else {
            return nil
        }
        return requestsViewContorllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        //currentControllerIndex(VC: viewController)
        guard let viewControllerIndex = requestsViewContorllers.index(of: viewController) else {
            return nil
        }
        let nextIndex = viewControllerIndex + 1
        guard requestsViewContorllers.count != nextIndex else {
            return nil
        }
        guard requestsViewContorllers.count > nextIndex else {
            return nil
        }
        return requestsViewContorllers[nextIndex]
    }
    
}
extension PageViewController : UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
            self.pageControl.currentPage = requestsViewContorllers.index(of: pageContentViewController)!
       //     PageViewController.self.pageDelegate?.changeCurrentPage(page:  requestsViewContorllers.index(of: pageContentViewController)!)
    }
}

