//
//  CustomerProfileViewController.swift
//  OonPayProject
//
//  Created by Ankita Patil on 06/11/20.
//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit

class CustomerProfileViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func actionBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

    @IBAction func actionCustomerNameBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "YourNameViewController") as! YourNameViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func actionAddressBtn(_ sender: Any) {
        let AddressVC = self.storyboard?.instantiateViewController(withIdentifier: "AddressViewController") as! AddressViewController
        self.navigationController?.pushViewController(AddressVC, animated: true)
    }
    
    
    
    
    
}
