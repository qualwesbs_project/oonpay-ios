//
//  EnterCreditTransactionViewController.swift
//  OonPayProject
//
//  Created by Ankita Patil on 05/11/20.
//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit

class EnterCreditTransactionViewController: UIViewController {
    
    //MARK: IBOutlets
    

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    

    @IBAction func actionBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionSaveBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EnterDebitTransactionViewController") as! EnterDebitTransactionViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
