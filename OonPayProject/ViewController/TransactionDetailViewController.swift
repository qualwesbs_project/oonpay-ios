//
//  TransactionDetailViewController.swift
//  OonPayProject
//
//  Created by Ankita Patil on 06/11/20.
//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit

class TransactionDetailViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func actionDownloadInVoiceBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TransactionViewController") as! TransactionViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
