//
//  EnterDetailsViewController.swift
//  OonPayProject
//
//  Created by Ankita Patil on 31/10/20.
//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit
import  CountryPickerView

class EnterDetailsViewController: UIViewController
,CountryPickerViewDataSource,CountryPickerViewDelegate {
    
//mark:- CountyPicker
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        print(country)
    }
    
 
    
 //Mark : - IBOutlets
    

    @IBOutlet weak var countryPicker: CountryPickerView!
    
    @IBOutlet weak var MobileNoField: DesignableUITextField!
    
    @IBOutlet weak var customerNameField: CustomTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        countryPicker.delegate=self
      countryPicker.dataSource=self
      countryPicker.showPhoneCodeInView = true

    }
    
//Mark:- IBAction-
    @IBAction func actionSaveBtn(_ sender: Any) {
        
        if (self.MobileNoField.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter Valid Mobile Number")
        }else if !(self.isValidPhone(phone: self.MobileNoField.text ?? "")) {
            Singleton.shared.showToast(text: "Enter Valid Mobile Number ")
        }
        ActivityIndicator.show(view: self.view)
        
        let param:[String:Any]=["customer_name" : customerNameField.text ?? "" ,
            "customer_mobile_number": MobileNoField.text ?? "" ]
            
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_ADD_CUSTOMER, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_ADD_CUSTOMER) { (response) in
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabBarViewController" ) as! TabBarViewController
        self.navigationController?.pushViewController(vc, animated: true)
        }
 
        
        
        
        
        
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EnterTransactionViewController") as!  EnterTransactionViewController
//        self.navigationController?.pushViewController(vc, animated:     true)
    }
    
    @IBAction func actionBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    
}
