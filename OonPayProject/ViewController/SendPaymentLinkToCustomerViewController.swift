//
//  SendPaymentLinkToCustomerViewController.swift
//  OonPayProject
//

//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit

class SendPaymentLinkToCustomerViewController: UIViewController {

    
 //Mark:-Outlets
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var SendPaymentLinkPopup: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource=self
        tableView.delegate=self

    }
//Mark:-Actions
    
    @IBAction func actionBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionPaymentBtn(_ sender: Any) {
        SendPaymentLinkPopup.isHidden=false
     }
    
    
}
extension SendPaymentLinkToCustomerViewController : UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:SendPaymentLinkListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SendPaymentLinkListTableViewCell") as! SendPaymentLinkListTableViewCell
        return cell
    }
    
    
}
