//
//  GenerateReceiptViewController.swift
//  OonPayProject
//

//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class GenerateReceiptViewController: UIViewController,SelectDate{
   
    func selectedDate(date: Int) {
        if(currentPicker == 1){
            self.receiptDateField.text = self.convertTimestampToDate(date, to: "dd-MM-yyyy")
        }else if(currentPicker == 2){
              self.DueDateField.text = self.convertTimestampToDate(date, to: "dd-MM-yyyy")
        }
    }
    
    @IBOutlet weak var receiptDateField: SkyFloatingLabelTextField!
    @IBOutlet weak var DueDateField: SkyFloatingLabelTextField!
    
    var currentPicker = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func presentDatePicker(mode:Int){
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerViewController") as! DatePickerViewController
        myVC.minDate = Date()
         myVC.dateDelegate = self
        myVC.pickerMode = 1
       // self.navigationController?.present(myVC, animated: true, completion: nil)
        self.present(myVC, animated: true, completion: nil)
        
    }
    
    @IBAction func recepitDateAction(_ sender: Any) {
        self.currentPicker = 1
        self.presentDatePicker(mode: 1)
    }
    
    @IBAction func dueDateAction(_ sender: Any) {
        self.currentPicker = 1
        self.presentDatePicker(mode: 1)
    }
    
    @IBAction func actionBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func actionSaveBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EnterTransactionViewController") as! EnterTransactionViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    
    
    
    
    
}
