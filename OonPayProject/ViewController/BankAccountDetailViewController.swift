//
//  BankAccountDetailViewController.swift
//  OonPayProject
//

//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit

class BankAccountDetailViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
//Mark:- IBActions
   
    @IBAction func actionAddAccountBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AccountAddedViewController") as! AccountAddedViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func actionBackbtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
