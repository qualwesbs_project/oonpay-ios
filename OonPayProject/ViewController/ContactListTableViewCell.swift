//
//  ContactListTableViewCell.swift
//  OonPayProject
//
//  Created by Ankita Patil on 17/10/20.
//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit

class ContactListTableViewCell: UITableViewCell {
    //MARK: IBOutlets
    @IBOutlet weak var userNumber: DesignableUILabel!
    @IBOutlet weak var userName: DesignableUILabel!
    @IBOutlet weak var userImage: UIImageView!
    
    var addContact:(()-> Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: IBActions
    @IBAction func addAction(_ sender: Any) {
        if let addContact = self.addContact{
            addContact()
        }
    }
    
}
