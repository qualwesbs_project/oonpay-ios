//
//  ManageDebitAndCreditViewController.swift
//  OonPayProject
//
//  Created by Ankita Patil on 04/11/20.
//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit

class ManageDebitAndCreditViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func actionNextBtn(_ sender: Any) {
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "SendNotificationViewController") as! SendNotificationViewController
        self.navigationController?.pushViewController(VC, animated: true)
    }
    

}
