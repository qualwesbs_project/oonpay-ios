//
//  MaintainLeaderViewController.swift
//  OonPayProject
//
//  Created by Ankita Patil on 04/11/20.
//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit

class MaintainLeaderViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionBtnNext(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "WatchVideoViewController") as! WatchVideoViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
