
import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func actionNextBtn(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ManageDebitAndCreditViewController") as! ManageDebitAndCreditViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

