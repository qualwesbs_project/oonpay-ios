//
//  SendPaymentLinkPopUpViewController.swift
//  OonPayProject
//
//  Created by Ankita Patil on 09/11/20.
//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit

protocol SendPaymentLinkDelegate{
    func presentSendPaymentLinkPopup()
}

class SendPaymentLinkPopUpViewController: UIViewController {
    var delegate: SendPaymentLinkDelegate?=nil

    override func viewDidLoad() {
        super.viewDidLoad()
     
    
    }
    

    @IBAction func actionSendPaymentLinkBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        self.delegate?.presentSendPaymentLinkPopup()
    }
    

}
