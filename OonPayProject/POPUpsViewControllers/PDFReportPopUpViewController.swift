//
//  PDFReportPopUpViewController.swift
//  OonPayProject
//

//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit

class PDFReportPopUpViewController: UIViewController , UITableViewDataSource , UITableViewDelegate {
 
    
    var DaysGrpArr : [String] = ["All","Last Week","Last Month","Date","Date Range"]

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource=self
        tableView.delegate=self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DaysGrpArr.count
     }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = UITableViewCell()
        let dayList = DaysGrpArr[indexPath.row]
        cell.textLabel!.text = "\(dayList)"
        return cell

    }
    

}
