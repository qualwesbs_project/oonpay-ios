//
//  CollectionPopUpViewController.swift
//  OonPayProject
//
//  Created by Ankita Patil on 07/11/20.
//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit

protocol CollectionPopupDelegate{
    func presentCollectionPopup()
}

class CollectionPopUpViewController: UIViewController {
    
var delegate:CollectionPopupDelegate? = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func actionStartNowBtn(_ sender: Any) {
    let vc = self.storyboard?.instantiateViewController(withIdentifier: "CollectionsViewController") as! CollectionsViewController
        self.delegate?.presentCollectionPopup()
    self.navigationController?.pushViewController(vc, animated: true)
   
        self.dismiss(animated: true, completion: nil)
       
        
    }
    

}
