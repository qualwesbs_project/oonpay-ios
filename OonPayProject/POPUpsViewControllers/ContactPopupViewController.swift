//
//  ContactPopupViewController.swift
//  OonPayProject
//
//  Created by Ankita Patil on 05/11/20.
//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit
protocol ContactPopupViewControllerDelegate {
    func presentContactPopup()
}

class ContactPopupViewController: UIViewController {
    
    var delegate : ContactPopupViewControllerDelegate?=nil

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    

    @IBAction func ActionOKBtn(_ sender: Any) {
        
       let vc =  self.storyboard?.instantiateViewController(withIdentifier: "AddNewContactViewController") as! AddNewContactViewController
         
        self.navigationController?.pushViewController(vc, animated: true)
          
        self.dismiss(animated: true, completion: nil)
        self.delegate?.presentContactPopup()
        

      
       
    }
    
    @IBAction func actionNotNowBtn(_ sender: Any) {
        
       self.dismiss(animated: true, completion: nil)
       
        
  
        
      
    }
    
}
