//
//  BusinessDetailViewController.swift
//  OonPayProject
//

//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit


class BusinessDetailViewController: UIViewController {
    var mobile:String?
    var PIN:String?
    
    //Mark:-IBOutlets
    @IBOutlet weak var businessNameField: DesignableUITextField!
    @IBOutlet weak var businessLocationField: DesignableUITextField!
    @IBOutlet weak var businessCategory: DesignableUITextField!
    @IBOutlet weak var businessEmail: DesignableUITextField!
    @IBOutlet weak var EnterNameField: DesignableUITextField!
    @IBOutlet weak var EnterEmailField: DesignableUITextField!
    @IBOutlet weak var businessContactField: DesignableUITextField!
    @IBOutlet weak var stackViewField: UIStackView!
    @IBOutlet weak var personalRadionImg: UIImageView!
    @IBOutlet weak var swainikRadioImg: UIImageView!
    @IBOutlet weak var personalRadioBtn: CustomButton!
    @IBOutlet weak var swainaikRadioBtn: CustomButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.personalRadionImg.image = #imageLiteral(resourceName: "selectedradiobtn")
        self.stackViewField.isHidden = true
    }
    
    func callUserType(){
        if (self.personalRadionImg.image == #imageLiteral(resourceName: "selectedradiobtn"))
        {
            //Add personal Api---
            ActivityIndicator.show(view: self.view)
            let param:[String:Any] = [
                    "name" : EnterNameField.text ?? "" ,
                    "customer_mobile_number": mobile ?? "",
                    "customer_name" : EnterNameField.text ?? "",
                    "customer_email":EnterEmailField.text ?? ""
                ]
            
            
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_Add_PERSONAL, method: .post,parameter: param, objectClass: AddPersonal.self, requestCode: U_Add_PERSONAL) { (response) in
                
                //   UserDefaults.standard.setValue(resp, forKey: UD_TOKEN)
                let userData = try! JSONEncoder().encode(response.response)
                print("userData--", userData)
                
                
                UserDefaults.standard.set(response.response?.personal_summary, forKey:UD_USER_DETAIL)
                ActivityIndicator.hide()
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabBarViewController" ) as! TabBarViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        //Add Business API---
        else{
            ActivityIndicator.show(view: self.view)
            let param:[String:Any]=["customer_mobile_number": mobile ?? "",
                                    "customer_name" : EnterNameField.text ?? "",
                                    "customer_email" : EnterEmailField.text ?? "",
                                    "business_name":businessNameField.text ?? "",
                                    "business_location":businessLocationField.text ?? "",
                                    "business_category":businessCategory.text ?? "",
                                    "business_email" : businessEmail.text ?? "",
                                    "business_contact": businessContactField.text ?? "" ]
            
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_ADD_BUSINESS, method: .post, parameter: param, objectClass: AddBusiness.self, requestCode: U_ADD_BUSINESS) { (response) in
                
                //  UserDefaults.standard.setValue(response.self, forKey: UD_TOKEN)
                let userData = try! JSONEncoder().encode(response.response)
                print("userData--", userData)
                
                UserDefaults.standard.set(userData, forKey:UD_USER_DETAIL)
                ActivityIndicator.hide()
                let vc1 = self.storyboard?.instantiateViewController(withIdentifier: "TabBarViewController" ) as! TabBarViewController
                self.navigationController?.pushViewController(vc1, animated: true)
            }
        }
    }
    
    
    //Mark:-IBActions
    @IBAction func actionPersonalRadioBtn(_ sender: Any) {
        self.swainikRadioImg.image = #imageLiteral(resourceName: "radiobutton")
        self.personalRadionImg.image = #imageLiteral(resourceName: "selectedradiobtn")
        self.stackViewField.isHidden=true
        
    }
    @IBAction func actionSwainaikRadioBtn(_ sender: Any) {
        self.personalRadionImg.image = #imageLiteral(resourceName: "radiobutton")
        self.swainikRadioImg.image = #imageLiteral(resourceName: "selectedradiobtn")
        self.stackViewField.isHidden=false
    }
    
    
    @IBAction func actionBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionBtnGetStarted(_ sender: Any) {
        if(self.EnterNameField.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter name")
        }else if(self.EnterEmailField.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter Email Address")
        }else if !(self.isValidEmail(emailStr: self.EnterEmailField.text ?? "")){
                Singleton.shared.showToast(text: "Enter valid Email Address")
        }else  if (self.personalRadionImg.image == #imageLiteral(resourceName: "selectedradiobtn")){
            ActivityIndicator.show(view: self.view)
            
            
            let param:[String:Any]=["mobile_number" :  mobile ?? "" ,
                                    "verification_pin" : PIN ?? "" ,
                                    "email" : EnterEmailField.text ?? "",
                                    "name" : EnterNameField.text ?? ""]
            
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_SIGNUP, method: .post, parameter: param, objectClass: Signup.self, requestCode: U_SIGNUP) { (response) in
                ActivityIndicator.hide()
                self.callUserType()
            }
        }else {
            if(self.businessNameField.text!.isEmpty){
                Singleton.shared.showToast(text: "Enter Business Name")
            }else if(self.businessLocationField.text!.isEmpty){
                Singleton.shared.showToast(text: "Enter Business Location")
            }else if(self.businessCategory.text!.isEmpty){
                Singleton.shared.showToast(text: "Enter Business Category")
            }else if(self.businessEmail.text!.isEmpty){
                Singleton.shared.showToast(text: "Enter Business Email")
            }else if(self.businessContactField.text!.isEmpty){
                Singleton.shared.showToast(text: "Enter Business Contact")
            }else{
                ActivityIndicator.show(view: self.view)
                
                let param:[String:Any]=["mobile_number" :  mobile ?? "" ,
                                        "verification_pin" : PIN ?? "" ,
                                        "email" : EnterEmailField.text ?? "",
                                        "name" : EnterNameField.text ?? ""]
                
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_SIGNUP, method: .post, parameter: param, objectClass: Signup.self, requestCode: U_SIGNUP) { (response) in
                    ActivityIndicator.hide()
                    self.callUserType()
                }
            }
        }
    }
    
}

