//
//  EnterCustomerNameViewController.swift
//  OonPayProject
//
//  Created by Ankita Patil on 05/11/20.
//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit

class EnterCustomerNameViewController: UIViewController {
    
    
    @IBOutlet weak var personalRadioImg: UIImageView!
    @IBOutlet weak var personalRadioBtn: CustomButton!
    @IBOutlet weak var swainiakRadioImg: UIImageView!
    @IBOutlet weak var swaniawikRadioBtn: CustomButton!
    
    override func viewDidLoad(){
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
  //IBAction---
    
    @IBAction func actionBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionPersonalRadioBtn(_ sender: Any) {
        if(personalRadioImg.image == UIImage(named: "radiobutton")){
          personalRadioImg.image = UIImage(named: "selectedradiobtn")
            let vc =  self.storyboard?.instantiateViewController(withIdentifier: "EnterCustomerNameViewController") as! EnterCustomerNameViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
         personalRadioImg.image = UIImage(named: "radiobutton")
        }
    }
    
    @IBAction func actionSwainaikRadioBtn(_ sender: Any) {
        if(swainiakRadioImg.image == UIImage(named: "radiobutton")){
          swainiakRadioImg.image = UIImage(named: "selectedradiobtn")
        }else {
         swainiakRadioImg.image = UIImage(named: "radiobutton")
        }
    }
    
    @IBAction func actionBtnGetStarted(_ sender: Any) {
      
        
    }
    
}
