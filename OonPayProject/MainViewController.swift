//
//  MainViewController.swift
//  OonPayProject
//
//  Created by qw on 27/11/20.
//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setInitialController()
        // Do any additional setup after loading the view.
    }
    
    func setInitialController(){
      
        
        var initialViewController = UIViewController()
        
        if let myVC = UserDefaults.standard.value(forKey: UD_IS_FIRST_TIME) as? String {
            if let view = UserDefaults.standard.value(forKey: UD_TOKEN) as? String{
                initialViewController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
            }else {
                initialViewController = self.storyboard?.instantiateViewController(withIdentifier: "ObtainPINViewController") as! ObtainPINViewController
            }
        }else {
            UserDefaults.standard.set("true", forKey: UD_IS_FIRST_TIME)
            initialViewController = self.storyboard?.instantiateViewController(withIdentifier:"ViewController") as! ViewController
        }
       
        self.navigationController?.pushViewController(initialViewController, animated: true)
    }

}
