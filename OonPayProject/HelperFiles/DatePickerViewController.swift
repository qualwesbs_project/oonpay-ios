//
//  DatePickerViewController.swift
//  OonPayProject
//
//  Created by Ankita Patil on 31/10/20.
//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit

protocol SelectDate {
    func selectedDate(date: Int)
}

class DatePickerViewController: UIViewController {

    @IBOutlet weak var datePicker: UIDatePicker!
    
    var dateDelegate:SelectDate? = nil
    var selectedDate = Int()
    var pickerMode = 1
    var minDate:Date? = nil
    var maxDate:Date? = nil
    var pickerDate: Date?
    var isPostJobTime = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(pickerMode == 1 || pickerMode == 4){
                self.datePicker.datePickerMode = .date
                if(minDate != nil){
                datePicker.minimumDate = Date()
                }
            }else if(pickerMode == 2){
                self.datePicker.datePickerMode = .time
            }else {
                self.datePicker.datePickerMode = .dateAndTime
            }
            if(pickerDate != nil){
                datePicker.date = pickerDate ?? Date()
            }
            if(maxDate != nil){
                datePicker.maximumDate = maxDate!
            }
            if(isPostJobTime){
                datePicker.minuteInterval = 30
            }
        }

    
    @IBAction func actionBtnDone(_ sender: Any) {
        let time = datePicker.date.timeIntervalSince1970
        self.dateDelegate?.selectedDate(date: Int(time))
        self.dismiss(animated: true, completion: nil)
    }
    
      
    }
    

    


