//
//  Model.swift
//  OonPayProject
//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit

struct SuccessResponse:Codable {
    var message : String?
    var status : Int?
}

struct ErrorResponse : Codable {
    var message : String?
    var status : Int?
}
struct login : Codable{
    var response : loginResponse?
    var message:String?
    var status:Int?
}

struct loginResponse : Codable{
    var token:String?
    var user : UserResponse
}
struct UserResponse : Codable {
    var id : Int?
    var name : String?
    var mobile_number : String?
    var profile_photo:String?
    var verification_pin:String?
    var email:String
}

struct Signup : Codable{
   var response : SignupResponse?
   var message : String?
   var status : Int?
}

struct SignupResponse : Codable{
    var mobile_number : String?
    var verification_pin : String?
    var email : String?
    var name : String?
    var token : String?
}

struct AddBusiness : Codable{
    var response:Response?
    var message:String?
    var status:Int?
}
struct Response:Codable{
    var business_Summary : [BusinessSummary]?
}
struct BusinessSummary : Codable{
    var id:Int?
    var customer_mobile_number : String?
    var customer_name:String?
    var customer_email:String?
    var business_name:String?
    var business_location:String?
    var business_category:String?
    var business_email:String?
    var business_contact:String?
}


struct AddPersonal : Codable{
    var response:PersonalResponse?
    var message:String?
    var status:String?
}
struct PersonalResponse : Codable {
    var personal_summary : [PersonalSummary]?
}
struct PersonalSummary : Codable {
    var id:Int?
    var name:String?
    var user_id:String?
    var created_at:String?
    var updated_at:String?
    var status: Int?
    var personal_location: String?
    var personal_contact: String?
}


struct SearchMyAccount:Codable{
    var response = SearchMyAccountResponse()
    var message:String?
    var status:Int?
}
struct SearchMyAccountResponse:Codable{
    var accounts = [Account]()
}

struct Account:Codable{
    var id:Int?
    var name:String?
    var userID:Int?
    var createdAt,updatedAt:String?
    
}

struct GetCustomer:Codable{
    var response = CustomerResponse()
    var message:String?
    var status:Int?
    
}

struct GetSingleCustomer: Codable{
   var response = SingleCustomerResponse()
    var message: String?
    var status: Int?
}

struct SingleCustomerResponse: Codable {
    var customer = [Customer]()
    var customer_products = [CustomerProduct]()
}

struct CustomerProduct: Codable {
    var id: Int?
    var customer_id: Int?
    var user_id: Int?
    var prduct_id: Int?
    var price: Int?
    var payment_mode: Int?
    var receipt_date: Int?
    var due_date: Int?
    var product_image: String?
    var credits: Int?
    var owns: Int?
    var pdf_url: String?
    var status: Int?
    var product_name: String?
    
}

struct CustomerResponse:Codable{
    var customers = [Customer]()
    var total_credits: String?
    var total_debits: String?
    
}
struct Customer:Codable{
    var customer_name : String?
    var customer_mobile_number: String?
    var user_id: Int?
    var updated_at : String?
    var created_at: String?
    var id: Int?
}

// edit business struct ---

struct EditBusiness:Codable{
    var response : EditBusinessResponse?
    var message : String?
    var status : Int?
}
struct EditBusinessResponse:Codable{
    var business:Business?
}
struct Business:Codable{
    var id:Int?
    var business_name:String?
    var business_location:String?
    var business_category:String?
    var business_email:String?
    var business_contact:String?
    var user_id:Int?
    var created_at:String?
    var updated_at:String?
    var business_profile_image:String?
}

struct UploadImage:Codable {
    var response:uploadImageResponse?
    var message:String?
    var status:Int?
    
}
struct uploadImageResponse:Codable{
    var image_path : String?
    var image_name : String?
    var original_image: String?
}


struct LocalContacts {
    var firstName: String
    var lastName: String
    var telephone: String
}

struct GetAllAccounts: Codable {
    var response = AllAccounts()
    var message: String?
    var status: Int?
}

struct AllAccounts: Codable {
    var business = [Business]()
    var personal = [PersonalSummary]()
}
