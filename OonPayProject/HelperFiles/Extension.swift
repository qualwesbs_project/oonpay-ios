//
//  Extension.swift
//  OonPayProject
//

//  Copyright © 2020 onpay. All rights reserved.
//

import Foundation
import UIKit
import SkyFloatingLabelTextField

public extension UIDevice {

    var iPhone: Bool {
        return UIDevice().userInterfaceIdiom == .phone
    }

    enum ScreenType: String {
        case iPhone4
        case iPhone5
        case iPhone6
        case iPhone6Plus
        case iPhoneX
        case Unknown
    }

    var screenType: ScreenType {
        guard iPhone else { return .Unknown}
        switch UIScreen.main.nativeBounds.height {
        case 960:
            return .iPhone4
        case 1136:
            return .iPhone5
        case 1334:
            return .iPhone6
        case 2208:
            return .iPhone6Plus
        case 2436:
            return .iPhoneX
        default:
            return .Unknown
        }
    }

}

extension UIViewController {
    func convertTimestampToDate(_ timestamp: Int, to format: String) -> String {
         let date = Date(timeIntervalSince1970: TimeInterval(timestamp))
         let dateFormatter = DateFormatter()
         dateFormatter.dateFormat = format
         dateFormatter.timeZone = TimeZone(abbreviation: "GMT-5")
         //dateFormatter.locale = TimeZone.current
         return dateFormatter.string(from: date)
     }
     
     func convertDateToTimestamp(_ date: String, to format: String) -> Int{
         let dfmatter = DateFormatter()
         dfmatter.dateFormat = format
          dfmatter.timeZone = TimeZone(abbreviation: "GMT-5")
         let date = dfmatter.date(from: date)
         var dateStamp:TimeInterval?
         if let myDate = date {
             dateStamp = myDate.timeIntervalSince1970
             let dateSt:Int = Int(dateStamp!)
             return dateSt
         }else {
             return Int(Date().timeIntervalSince1970)
         }
     }
    
    
    
    func showAlert(title: String?, message: String?, action1Name: String?, action2Name: String?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        //alertController.view.backgroundColor = lightPink
        //        alertController.title?.withBoldText(text: <#T##String#>, font: <#T##UIFont?#>)
        alertController.addAction(UIAlertAction(title: action1Name, style: .default, handler: nil))
        if action2Name != nil {
            alertController.addAction(UIAlertAction(title: action2Name, style: .default, handler: nil))
        }
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    
    
    func showToast(message : String,isAddToBag:Bool) {
        let toastLabel = UILabel(frame: CGRect.zero)
        toastLabel.font = UIFont(name: "Roboto-Regular", size: 14.0)
        toastLabel.text = message
        toastLabel.sizeToFit()
        toastLabel.frame.size = CGSize(width: toastLabel.frame.width + 30, height: toastLabel.frame.height + 20)
        if(UIScreen.main.bounds.height > 700){
            toastLabel.center = CGPoint(x: self.view.center.x, y: self.view.frame.maxY - 110)
        }else {
            toastLabel.center = CGPoint(x: self.view.center.x, y: self.view.frame.maxY - 70)
        }
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(1)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 5
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 0, delay: 1.5, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    
    func isValidPhone(phone: String) -> Bool {
        let phoneRegex = "^[0-9+]{0,1}+[0-9]{5,16}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        return phoneTest.evaluate(with: phone)
    }
    
    func isValidEmail(emailStr:String) -> Bool {
          let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
          
          let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
          return emailPred.evaluate(with: emailStr)
      }
    
    
}
extension UIColor {
    public convenience init?(hex: String) {
        let r, g, b, a: CGFloat

        if hex.hasPrefix("#") {
            let start = hex.index(hex.startIndex, offsetBy: 1)
            let hexColor = String(hex[start...])

            if hexColor.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0

                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255

                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }

        return nil
    }
}

class CustomTextField: SkyFloatingLabelTextField {
    private var _titleFormatter: (String) -> String = { (text: String) -> String in
        return text
    }
    override var titleFormatter: ((String) -> String) {
        get {
            return _titleFormatter
        }
        set {
            
        }
    }
}
