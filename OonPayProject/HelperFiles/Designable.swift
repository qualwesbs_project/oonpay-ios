//
//  Designable.swift
//  OonPayProject
//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit
@IBDesignable
class CustomButton: UIButton {
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return self.borderWidth
        } set {
            self.layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.borderColor!)
        } set {
            self.layer.borderColor = newValue?.cgColor
        }
    }
    
    @IBInspectable var fontSize: Int {
        get {
            return self.fontSize
        } set {
            self.titleLabel?.font = changeFont(val: newValue)
            
        }
    }
    
    
    @IBInspectable var circle: Bool {
        get {
            return self.circle
        } set {
            if newValue {
                self.layer.cornerRadius = self.frame.width/2
                self.clipsToBounds = true
            }
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return self.cornerRadius
        } set {
            self.layer.cornerRadius = newValue
            self.clipsToBounds = true
        }
    }
    
    @IBInspectable var shadowColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.shadowColor!)
        } set {
            self.layer.shadowColor = newValue?.cgColor
        }
    }
    
    @IBInspectable var shadowOpacity: Float {
        get {
            return self.layer.shadowOpacity
        } set {
            self.layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable var shadowOffset: CGSize {
        get {
            return self.layer.shadowOffset
        } set {
            self.layer.shadowOffset = newValue
        }
    }
}

@IBDesignable
class View: UIView {
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return self.borderWidth
        } set {
            self.layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.borderColor!)
        } set {
            self.layer.borderColor = newValue?.cgColor
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return self.cornerRadius
        } set {
            self.layer.cornerRadius = newValue
            self.clipsToBounds = true
        }
    }
    
    @IBInspectable var topCornorRound: CGFloat {
        get {
            return self.topCornorRound
        } set {
            self.clipsToBounds = false
            self.layer.cornerRadius = newValue
            self.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        }
    }
    
    @IBInspectable var bottomCornorRound: CGFloat {
        get {
            return topCornorRound
        } set {
            self.clipsToBounds = false
            self.layer.masksToBounds = false
            self.layer.cornerRadius = newValue
            self.layer.maskedCorners =  [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        }
    }
    
    @IBInspectable var circle: Bool {
        get {
            return self.circle
        } set {
            if newValue {
                self.layer.cornerRadius = self.frame.width/2
                self.clipsToBounds = true
            }
        }
    }
    
    @IBInspectable var shadowColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.shadowColor!)
        } set {
            self.layer.shadowColor = newValue?.cgColor
            self.layer.masksToBounds = false
        }
    }
    
    @IBInspectable var shadowOpacity: Float {
        get {
            return self.layer.shadowOpacity
        } set {
            
            self.layer.shadowOpacity = newValue
            
        }
    }
    
    @IBInspectable var shadowOffset: CGSize {
        get {
            return self.layer.shadowOffset
        } set {
            
            self.layer.shadowOffset = newValue
        }
    }
}

@IBDesignable
class ImageView: UIImageView {
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return self.borderWidth
        } set {
            self.layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.borderColor!)
        } set {
            self.layer.borderColor = newValue?.cgColor
        }
    }
    @IBInspectable var shadowColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.shadowColor!)
        } set {
            self.layer.shadowColor = newValue?.cgColor
        }
    }
    
    @IBInspectable var shadowOpacity: Float {
        get {
            return self.layer.shadowOpacity
        } set {
            self.layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable var shadowOffset: CGSize {
        get {
            return self.layer.shadowOffset
        } set {
            self.layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return self.cornerRadius
        } set {
            self.layer.cornerRadius = newValue
            self.clipsToBounds = true
        }
    }
    
    @IBInspectable var circle: Bool {
        get {
            return self.circle
        } set {
            if newValue {
                self.layer.cornerRadius = self.frame.height/2
                self.clipsToBounds = true
                self.layer.masksToBounds = true
                self.contentMode = .scaleAspectFill
            }
        }
    }
    
    @IBInspectable var customTintColor: UIColor {
        get {
            return self.customTintColor
        } set {
            let templateImage =  self.image?.withRenderingMode(.alwaysTemplate)
            self.image = templateImage
            self.tintColor = newValue
        }
    }
}

@IBDesignable
class DesignableUITextField: UITextField {
    
    // Provides left padding for images
    
    @IBInspectable var leftPadding: CGFloat = 0
    @IBInspectable var rightPadding: CGFloat = 0
    
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.leftViewRect(forBounds: bounds)
        textRect.origin.x += leftPadding
        return textRect
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return self.borderWidth
        } set {
            self.layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var fontSize: Int {
        get {
            return self.fontSize
        } set {
            self.font = changeFont(val: newValue)
            
        }
    }
    
    @IBInspectable var circle: Bool {
          get {
            return self.circle
          } set {
              if newValue {
                  self.layer.cornerRadius = self.frame.width/2
                  self.clipsToBounds = true
              }
          }
      }
    
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.borderColor!)
        } set {
            self.layer.borderColor = newValue?.cgColor
        }
    }
    
    //corner radius
    @IBInspectable var cornerRadius: CGFloat {
           get {
               return cornerRadius
           } set {
               self.layer.cornerRadius = newValue
               self.clipsToBounds = true
           }
       }
       
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.rightViewRect(forBounds: bounds)
        textRect.origin.x -= rightPadding
        return textRect
    }
    
    @IBInspectable var leftImage: UIImage? {
        didSet {
            // updateView()
        }
    }
    
    @IBInspectable var rightImage: UIImage? {
        didSet {
            //  updateView()
        }
    }
    
    @IBInspectable var shadowColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.shadowColor!)
        } set {
            self.layer.shadowColor = newValue?.cgColor
        }
    }
    
    @IBInspectable var shadowOpacity: Float {
        get {
            return self.layer.shadowOpacity
        } set {
            self.layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable var shadowOffset: CGSize {
        get {
            return self.layer.shadowOffset
        } set {
            self.layer.shadowOffset = newValue
        }
    }
}


@IBDesignable
class DesignableUILabel: UILabel {
    
    // Provides left padding for images
    
    @IBInspectable var leftPadding: CGFloat = 0
    @IBInspectable var rightPadding: CGFloat = 0
    
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return self.borderWidth
        } set {
            self.layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var circle: Bool {
        get {
            return self.circle
        } set {
            if newValue {
                self.layer.cornerRadius = self.frame.height/2
                self.clipsToBounds = true
                self.layer.masksToBounds = true
                self.contentMode = .scaleAspectFill
            }
        }
    }
    
    @IBInspectable var fontSize: Int {
        get {
            return self.fontSize
        } set {
            self.font = changeFont(val: newValue)
            
        }
    }
    
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.borderColor!)
        } set {
            self.layer.borderColor = newValue?.cgColor
        }
    }
    
    
    @IBInspectable var shadowOpacity: Float {
        get {
            return self.layer.shadowOpacity
        } set {
            self.layer.shadowOpacity = newValue
        }
    }
    
}


func changeFont(val: Int) -> UIFont {
    switch val {
        
    //FOR Main Title
    case 1:
        let lab = UILabel()
        switch UIDevice().screenType {
        case .iPhone4, .iPhone5:
            lab.font = UIFont(name: "Roboto-Bold", size:26)
            return lab.font
            
        case .iPhone6Plus, .iPhoneX:
            lab.font = UIFont(name: "Roboto-Bold", size:34)
            return lab.font
        default:
            lab.font = UIFont(name:"Roboto-Bold", size:30)
            return lab.font
        }
    //FOR Main Sub-heading
    case 2:
        let lab = UILabel()
        switch UIDevice().screenType {
        case .iPhone4, .iPhone5:
            lab.font = UIFont(name: "Roboto-Bold", size:24)
            return lab.font
            
        case .iPhone6Plus, .iPhoneX:
            lab.font = UIFont(name: "Roboto-Bold", size:28)
            return lab.font
        default:
            lab.font = UIFont(name: "Roboto-Bold", size:26)
            return lab.font
        }
    case 3:
        let lab = UILabel()
        switch UIDevice().screenType {
        case .iPhone4, .iPhone5:
            lab.font = UIFont(name: "Roboto-Medium", size:15)
            return lab.font
        case .iPhone6Plus, .iPhoneX:
            lab.font = UIFont(name: "Roboto-Medium", size:17)
            return lab.font
        default:
            lab.font = UIFont(name: "Roboto-Medium", size:19)
            return lab.font
        }
    case 4:
        let lab = UILabel()
        switch UIDevice().screenType {
        case .iPhone4, .iPhone5:
            lab.font = UIFont(name: "Roboto-Regular", size:14)
            return lab.font
            
        case .iPhone6Plus, .iPhoneX:
            lab.font = UIFont(name: "Roboto-Regular", size:16)
            return lab.font
        default:
            lab.font = UIFont(name: "Roboto-Regular", size:16)
            return lab.font
        }
    case 5:
        let lab = UILabel()
        switch UIDevice().screenType {
        case .iPhone4, .iPhone5:
            return UIFont(name: "Roboto-Regular", size:12)!
            default:
                return UIFont(name: "Roboto-Regular", size:12)!
            }
       
    default:
        return UIFont(name: "Roboto-Regular", size: 12)!
        
    }
}


