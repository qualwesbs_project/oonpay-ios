//
//  Constant.swift
//  OonPayProject

//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit

//COLORS
let primaryColor=UIColor(red: 50/255, green: 110/255, blue: 200/255, alpha: 1) //#326EC8
let orangeColor=UIColor(red: 255/255, green: 153/255, blue: 34/255, alpha: 1) //#ff9922
let greyfadedColor=UIColor(red: 139, green: 237, blue: 237, alpha: 1) //#EFEDED

//API's
let U_BASE = "http://54.219.89.116/onnpay_api/api/"
let U_LOGIN = "login"
let U_SIGNUP = "sign-up"
let U_ADD_BUSINESS = "add-business"
let U_Add_PERSONAL = "add-personal"
let U_SEARCH_ACCOUNT = "search-my-accounts"
let U_ADD_CUSTOMER = "add-customer"
let U_GET_CUSTOMER = "get-customers"
let U_UPLOAD_IMAGE = "upload-image"
let U_EDIT_BUSINESS = "edit-business/2"
let U_GET_SINGLE_CUSTOMER = "single-customer/"
let U_GET_ALL_ACCOUNTS = "get-all-accounts"



//User Default
var UD_TOKEN = "token"
var UD_USER_DETAIL = "user_detail"
var UD_PIN = "pin"
var UD_MOBILE_NO = "mobileNo"
var UD_INITIAL_CONTROLLER = "ObtainPINViewController"
var UD_IS_FIRST_TIME = "launch_first_time"
var UD_SELECTED_ACCOUNT = "selected_account"

//Notification Center
let N_LOGIN = "user_login"

