//
//  ActivityIndicator.swift
//  OonPayProject
//
//  Created by Ankita Patil on 11/11/20.
//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit
class ActivityIndicator
{
    static var overlayView = UIView()
    static var activityIndicator = UIActivityIndicatorView()
    
    static func show(view: UIView) {
        DispatchQueue.main.async {
            self.overlayView = view
            self.overlayView.isUserInteractionEnabled = false
            activityIndicator.center = view.center
            activityIndicator.hidesWhenStopped = true
            activityIndicator.startAnimating()
            activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
            activityIndicator.color = primaryColor
            view.addSubview(activityIndicator)
        }
    }
    
    static func hide() {
        overlayView.isUserInteractionEnabled = true
        activityIndicator.stopAnimating()
    }
}

