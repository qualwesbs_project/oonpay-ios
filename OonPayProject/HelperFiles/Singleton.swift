//
//  Singleton.swift
//  OonPayProject
//
//  Created by Ankita Patil on 11/11/20.
//  Copyright © 2020 onpay. All rights reserved.
//

import Foundation
import UIKit
import Toaster



class Singleton {
    
    static let shared = Singleton()
    var userDetail = SignupResponse()
    var selectedAccount = Account()
    var allAccounts = AllAccounts()
    
    public static func getUser() -> SignupResponse{
        if let info = UserDefaults.standard.data(forKey: UD_USER_DETAIL){
            let userData = info
            let userArray = try! JSONDecoder().decode(SignupResponse.self, from: userData)
            return userArray
        }else {
            return SignupResponse()
        }
    }
    
    func initialiseValues(){
        
    }
    
    func showToast(text: String?){
        if (ToastCenter.default.currentToast?.text == text) {
            return
        }
        if(UIScreen.main.bounds.height > 700){
            ToastView.appearance().bottomOffsetPortrait = 110
        }else {
            ToastView.appearance().bottomOffsetPortrait = 70
        }
        
        ToastView.appearance().textColor = .white
        ToastView.appearance().backgroundColor = UIColor(hue: 0, saturation: 0, brightness: 0, alpha: 0.7)
        ToastView.appearance().font = UIFont(name: "Roboto-Regular", size: 17)
        
        Toast(text: text, delay: 0, duration: 2).show()
        Toast(text: text, delay: 0, duration: 2).cancel()
    }
}
