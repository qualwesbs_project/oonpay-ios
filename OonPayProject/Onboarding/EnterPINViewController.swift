//
//  EnterPINViewController.swift
//  OonPayProject
//

//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit

class EnterPINViewController: UIViewController {
    var mobile:String?
    
    //Mark:- OBOutlet
    
    @IBOutlet weak var PINField: DesignableUITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleLoginNavigation(_:)), name: NSNotification.Name(rawValue: N_LOGIN), object: nil)
    }
    //Notification function-
    @objc func handleLoginNavigation(_ notif:Notification){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: N_LOGIN), object: nil)
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BusinessDetailViewController") as! BusinessDetailViewController
        
        NotificationCenter.default.addObserver(self, selector:#selector(self.handleLoginNavigation(_:)), name: NSNotification.Name(rawValue: N_LOGIN), object: nil)
        
        vc.PIN=self.PINField.text!
        vc.mobile=self.mobile!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //Mark:- IBAction
    @IBAction func actionEnterPINBtn(_ sender: Any) {
        if(self.PINField.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter pin")
        }else {
            ActivityIndicator.show(view: self.view)
            let param:[String:Any] = [
                "mobile_number" :  mobile ?? "",
                "verification_pin" : PINField.text ?? ""
            ]
            
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_LOGIN, method: .post, parameter: param, objectClass: login.self, requestCode: U_LOGIN) {(response) in
                let userData = try! JSONEncoder().encode(response.response)
                UserDefaults.standard.setValue(userData, forKey: UD_USER_DETAIL)
                UserDefaults.standard.setValue(response.response?.token ?? "", forKey: UD_TOKEN)
                ActivityIndicator.hide()
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectRoleViewController") as! SelectRoleViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    @IBAction func actionBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

