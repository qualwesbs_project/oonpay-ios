//
//  ObtainPINViewController.swift
//  OonPayProject
//

//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit
import CountryPickerView
import SkyFloatingLabelTextField

class ObtainPINViewController: UIViewController, CountryPickerViewDelegate,CountryPickerViewDataSource {
    
    //Mark:IBOutlets
    
    @IBOutlet weak var CountryPickerView: CountryPickerView!
    @IBOutlet weak var mobileNoField: SkyFloatingLabelTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CountryPickerView.delegate=self
        CountryPickerView.dataSource=self
        CountryPickerView.showPhoneCodeInView = true
    }
    
    //Mark:- IBActions
    @IBAction func actionGetPinBtn(_ sender: Any) {
        if (self.mobileNoField.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter  Mobile Number")
        }else if !(self.isValidPhone(phone: self.mobileNoField.text ?? "")){
            Singleton.shared.showToast(text: "Enter Valid Mobile Number")
            Singleton.shared.showToast(text: "Enter Valid Mobile Number")
        }else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "EnterPINViewController") as! EnterPINViewController
            vc.mobile = self.mobileNoField.text ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //Mark - CountryPickerViewFunction
    
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        print(country)
        
    }
    
}
