//
//  EnterTransactionViewController.swift
//  OonPayProject
//

//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit

class EnterTransactionViewController: UIViewController, ChangeAccount {
    func changeCurrentAccount(accountId: Int?, accountType: Int?) {
        
    }
    
    func dismissPopup() {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "BusinessDetailViewController") as! BusinessDetailViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
    //MARK: IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var totalCredits: DesignableUILabel!
    @IBOutlet weak var totalDebits: DesignableUILabel!
    
    var customerData = [Customer]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate=self
        tableView.dataSource=self
        tableView.tableFooterView = UIView()
        self.getCustomer()
    }
    
    func getCustomer(){
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_CUSTOMER, method: .post, parameter: nil, objectClass: GetCustomer.self, requestCode: U_GET_CUSTOMER) { (response)in
            
            self.customerData = response.response.customers
            self.totalCredits.text = "₦\(response.response.total_credits ?? "")"
            self.totalDebits.text = "₦\(response.response.total_debits ?? "")"
            
            self.tableView.reloadData()
            ActivityIndicator.hide()
            
        }
    }
    
    
    
    //MARK: IBActions
    @IBAction func actionBtnAddContact(_ sender: Any) {
        let  vc = self.storyboard?.instantiateViewController(withIdentifier: "AddContactViewController") as! AddContactViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionCancelBtn(_ sender: Any) {
    
    }
    
    @IBAction func actionDocCancelBtn(_ sender: Any) {
    }
    
    @IBAction func actionDocBtn(_ sender: Any) {
        
        
    }
    
    @IBAction func actionCreateOonpayCancelBtn(_ sender: Any) {
    }
    
    @IBAction func actionCreateOonpayBtn(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddAccountViewController") as! AddAccountViewController
        myVC.accountDelegate = self
        self.navigationController?.present(myVC, animated: true, completion: nil)
        
    }
    
    @IBAction func actionSetCollectiondateBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SetCollectionDateViewController") as! SetCollectionDateViewController
        
        self.navigationController?.pushViewController(vc, animated:true)
    }
    
    @IBAction func actionViewReportBtn(_ sender: Any) {
    }
    
    @IBAction func actionNotificationBtn(_ sender: Any) {
        let NotificationVC = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(NotificationVC, animated: true)
        
    }
    
    
    @IBAction func actionProfileBtn(_ sender: Any) {
        let CustomerProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "CustomerProfileViewController") as! CustomerProfileViewController
        self.navigationController?.pushViewController(CustomerProfileVC, animated: true)
    }
}


extension EnterTransactionViewController:UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return customerData.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionListTableViewCell") as! TransactionListTableViewCell
        cell.nameLabel.text = self.customerData[indexPath.row].customer_name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
        vc.customer = self.customerData[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
