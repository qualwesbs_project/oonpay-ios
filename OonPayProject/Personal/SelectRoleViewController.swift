//
//  SelectRoleViewController.swift
//  OonPayProject
//
//  Copyright © 2020 onpay. All rights reserved.

import UIKit
import iOSDropDown

class SelectRoleViewController: UIViewController, UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.callSearchAPI()
        return true
    }
    
    
    //IBOutlets--
    @IBOutlet weak var personalRadioBtnImg: UIImageView!
    @IBOutlet weak var swainaikRadioBtnImg: UIImageView!
    @IBOutlet weak var businessName: DropDown!
    @IBOutlet weak var personalButton: CustomButton!
    @IBOutlet weak var businessButton: CustomButton!
    
    var selectedType:Int = 0
    var businessData = [Account]()
    var selectedBusiness = Account()
    var isListHidden = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.actionPersonalRadioBtn(self)
        self.businessName.delegate = self
        self.businessName.didSelect { (val, id, index) in
            self.selectedBusiness = self.businessData[id]
            self.businessName.hideList()
            self.isListHidden = true
        }
    }
    
    func callSearchAPI(){
        let param:[String:Any]=[
            "name" : self.businessName.text ?? "" ,
            "type": selectedType
        ]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_SEARCH_ACCOUNT, method: .post, parameter: param, objectClass: SearchMyAccount.self, requestCode: U_SEARCH_ACCOUNT) { (response) in
            self.businessData = response.response.accounts
            self.businessName.optionArray = []
            
            for val in self.businessData{
                self.businessName.optionArray.append(val.name ?? "")
                //self.businessName.op
            }
            if(self.isListHidden){
                self.isListHidden = false
                self.businessName.showList()
            }
        }
    }
    
    //IBAction--
    @IBAction func actionPersonalRadioBtn(_ sender: Any) {
        self.swainaikRadioBtnImg.image = #imageLiteral(resourceName: "radiobutton")
        self.personalRadioBtnImg.image = #imageLiteral(resourceName: "selectedradiobtn")
        selectedType = 2
    }
    @IBAction func actionSwainaikRadioBtn(_ sender: Any) {
        self.personalRadioBtnImg.image = #imageLiteral(resourceName: "radiobutton")
        self.swainaikRadioBtnImg.image = #imageLiteral(resourceName: "selectedradiobtn")
        selectedType = 1
        
    }
    
    @IBAction func actionBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionSearchBtn(_ sender: Any) {
        if(self.selectedBusiness.id == nil){
            Singleton.shared.showToast(text: "Select business")
        }else {
        let account = try? JSONEncoder().encode(self.selectedBusiness)
        UserDefaults.standard.setValue(account, forKey: UD_SELECTED_ACCOUNT)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabBarViewController" ) as! TabBarViewController
        self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

