//
//  AddContactViewController.swift
//  OonPayProject
//
//  Created by Ankita Patil on 04/11/20.
//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit
import ContactsUI

enum ContactsFilter {
    case none
    case mail
    case message
}


class AddContactViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var contactTable: UITableView!
    
    var contactData = [PhoneContact]()
    var filter: ContactsFilter = .none
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadContacts(filter: .none)
    }
    
    func phoneNumberWithContryCode() -> [String] {
        let contacts = self.getContacts()
        var arrPhoneNumbers = [String]()
        for contact in contacts {
            for ContctNumVar: CNLabeledValue in contact.phoneNumbers {
                if let fulMobNumVar  = ContctNumVar.value as? CNPhoneNumber {
                    if let MccNamVar = fulMobNumVar.value(forKey: "digits") as? String {
                        arrPhoneNumbers.append(MccNamVar)
                    }
                }
            }
        }
        return arrPhoneNumbers // here array has all contact numbers.
    }
    
    func getContacts(filter: ContactsFilter = .none) -> [CNContact] {
        
        let contactStore = CNContactStore()
        let keysToFetch = [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
            CNContactPhoneNumbersKey,
            CNContactEmailAddressesKey,
            CNContactThumbnailImageDataKey] as [Any]
        
        var allContainers: [CNContainer] = []
        do {
            allContainers = try contactStore.containers(matching: nil)
        } catch {
            print("Error fetching containers")
        }
        
        var results: [CNContact] = []
        
        for container in allContainers {
            let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
            
            do {
                let containerResults = try contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch as! [CNKeyDescriptor])
                results.append(contentsOf: containerResults)
            } catch {
                print("Error fetching containers")
            }
        }
        return results
    }
    
    func loadContacts(filter: ContactsFilter) {
        contactData.removeAll()
        var allContacts = [PhoneContact]()
        for contact in self.getContacts(filter: filter) {
            allContacts.append(PhoneContact(contact: contact))
        }
        
        var filterdArray = [PhoneContact]()
        if self.filter == .mail {
            filterdArray = allContacts.filter({ $0.email.count > 0 }) // getting all email
        } else if self.filter == .message {
            filterdArray = allContacts.filter({ $0.phoneNumber.count > 0 })
        } else {
            filterdArray = allContacts
        }
        contactData.append(contentsOf: filterdArray)
        
        for contact in contactData {
            print("Name -> \(contact.name)")
            print("Email -> \(contact.email)")
            print("Phone Number -> \(contact.phoneNumber)")
        }
        let arrayCode  = self.phoneNumberWithContryCode()
        for codes in arrayCode {
            print(codes)
        }
        DispatchQueue.main.async {
            self.contactTable.reloadData() // update your tableView having phoneContacts array
        }
    }
    
    
    
    //MARK: IBActions
    @IBAction func actionAddContactBtn(_ sender: Any) {
        self.popupView.isHidden = false
    }
    
    @IBAction func actionBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func notNowAction(_ sender: Any) {
        self.popupView.isHidden = true
    }
    
    @IBAction func okAction(_ sender: Any) {
        self.popupView.isHidden = true
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddNewContactViewController") as! AddNewContactViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension AddContactViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactListTableViewCell") as! ContactListTableViewCell
        let val = self.contactData[indexPath.row]
        cell.userName.text = val.name
        if let data = val.avatarData{
            cell.userImage.image = UIImage(data: data)
        }
        if(val.phoneNumber.count > 0){
            cell.userNumber.text = val.phoneNumber[0]
        }
        cell.addContact = {
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddNewContactViewController") as! AddNewContactViewController
            myVC.selectedContact = self.contactData[indexPath.row]
            self.navigationController?.pushViewController(myVC, animated: true)
        }
        return cell
    }
}

class PhoneContact: NSObject {
    
    var name: String?
    var avatarData: Data?
    var phoneNumber: [String] = [String]()
    var email: [String] = [String]()
    var isSelected: Bool = false
    var isInvited = false
    
    init(contact: CNContact) {
        name        = contact.givenName + " " + contact.familyName
        avatarData  = contact.thumbnailImageData
        for phone in contact.phoneNumbers {
            phoneNumber.append(phone.value.stringValue)
        }
        for mail in contact.emailAddresses {
            email.append(mail.value as String)
        }
    }
    
    override init() {
        super.init()
    }
}
