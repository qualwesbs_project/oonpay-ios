//
//  AddDetailsViewController.swift
//  OonPayProject
//

//  Copyright © 2020 onpay. All rights reserved.
//

import UIKit
 
class AddDetailsViewController: UIViewController {
   
//Mark:-IBOutlets
   
    @IBOutlet weak var nameField: DesignableUITextField!
    @IBOutlet weak var emailField: DesignableUITextField!
    @IBOutlet weak var personalRadionImg:
    UIImageView!
    @IBOutlet weak var swainwikRadiobtn: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
//Mark:-IBActions
    @IBAction func actionPersonalRadioBtn(_ sender: UIButton) {
        
       if(personalRadionImg.image == UIImage(named: "radiobutton")){
              personalRadionImg.image = UIImage(named: "selectedradiobtn")
            }else {
             personalRadionImg.image = UIImage(named: "radiobutton")
            }
           
        }
        
    @IBAction func actionSwaniawskiRadioBtn(_ sender: UIButton) {
        if(swainwikRadiobtn.image == UIImage(named: "radiobutton")){
          swainwikRadiobtn.image = UIImage(named: "selectedradiobtn")
        }else {
         swainwikRadiobtn.image = UIImage(named: "radiobutton")
        }
        
    }
    @IBAction func actionGetStartedBtn(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BusinessDetailViewController") as! BusinessDetailViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    

}
