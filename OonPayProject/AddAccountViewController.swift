//
//  AddAccountViewController.swift
//  OonPayProject
//
//  Created by qw on 14/01/21.
//  Copyright © 2021 onpay. All rights reserved.
//

import UIKit

protocol ChangeAccount {
    func changeCurrentAccount(accountId: Int?, accountType: Int?)
    func dismissPopup()
}

class AddAccountViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var accountTable: UITableView!
    @IBOutlet weak var popupView: UIView!
    
    
    var allAccounts = AllAccounts()
    var currentSelection = Int()
    var accountDelegate:ChangeAccount?  = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       getAllAccount()
    }
    
    func getAllAccount(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_ALL_ACCOUNTS, method: .get, parameter: nil, objectClass: GetAllAccounts.self, requestCode: U_GET_ALL_ACCOUNTS) { (response) in
            self.allAccounts = response.response
            Singleton.shared.allAccounts = response.response
            self.accountTable.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    //MARK: IBActions
    @IBAction func dismissView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func createNewAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        self.accountDelegate?.dismissPopup()
    }
    
    @IBAction func personalAction(_ sender: Any) {
        self.currentSelection = 1
        self.popupView.isHidden = false
    }
    
    @IBAction func businessAction(_ sender: Any) {
        self.currentSelection = 2
        self.popupView.isHidden = true
    }
    
}

extension AddAccountViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.currentSelection == 1){
            return self.allAccounts.personal.count
        }else if(self.currentSelection == 2){
            return self.allAccounts.business.count
        }else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AccountTableCell") as! AccountTableCell
        if(self.currentSelection == 1){
            cell.accountName.text = self.allAccounts.personal[indexPath.row].name
        }else if(self.currentSelection == 1){
            cell.accountName.text = self.allAccounts.business[indexPath.row].business_name
        }
        cell.selectBtn = {
            if(self.currentSelection == 1){
                self.accountDelegate?.changeCurrentAccount(accountId: self.allAccounts.personal[indexPath.row].id ?? 0, accountType: 1)
                let account = try? JSONEncoder().encode(Account(id: self.allAccounts.personal[indexPath.row].id ?? 0, name: self.allAccounts.personal[indexPath.row].name ?? "", userID: 0, createdAt: nil, updatedAt: nil))
                UserDefaults.standard.setValue(account, forKey: UD_SELECTED_ACCOUNT)
            }else if(self.currentSelection == 2){
                self.accountDelegate?.changeCurrentAccount(accountId: self.allAccounts.business[indexPath.row].id ?? 0, accountType: 2)
                let account = try? JSONEncoder().encode(Account(id: self.allAccounts.business[indexPath.row].id ?? 0, name: self.allAccounts.business[indexPath.row].business_name ?? "", userID: 0, createdAt: nil, updatedAt: nil))
                UserDefaults.standard.setValue(account, forKey: UD_SELECTED_ACCOUNT)
            }
           
            self.dismiss(animated: true, completion: nil)
        }
        return cell
    }
}


class AccountTableCell: UITableViewCell{
    //MARK: IBOutlets
    @IBOutlet weak var accountName: UILabel!
    
    var selectBtn:(()-> Void)? = nil
    
    
    //MARK: IBActions
    @IBAction func selectAction(_ sender: Any) {
        if let selectBtn = self.selectBtn{
            selectBtn()
        }
    }
    
}
